# WhiskyMHD BNS merger waveform sample

## Description

The present repo contains gravitational waveforms extracted from numerical relativity simulations of binary neutron star mergers ("waveform" folder) and a set of corresponding waveforms where the post-merger part of the signal has been removed ("waveform-cut" folder).

Each folder contains a "readme" text file with the relevant information on the different models.

## Credits

Waveform usage is free. Please cite the source (repo link https://bitbucket.org/ciolfir/bns-waveforms, creator and maintainer R. Ciolfi).
Citation of the specific papers describing the corresponding simulations is welcome (not mandatory).

