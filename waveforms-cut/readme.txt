— LIST OF CUT WAVEFORMS —  (CUT means that the post-merger part is eliminated by hand)

SMNS or stable remnant:

- APR4_q10, APR4_q09, MS1_q10, MS1_q09 (magnetized, Emag=2.42e47 erg, Bmax=(2-3)e15 G)
   [from Ciolfi et al. 2017, https://arxiv.org/abs/1701.08738]
- LM_B0, UM_B0 (unmagnetized, Bmax=0)
   [from Endrizzi et al. 2016, http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1604.03445]
- SHT (unmagnetized, Bmax=0) 
   [from Kastaun et al. 2016, http://adsabs.harvard.edu/abs/2016PhRvD..94d4060K]

HMNS remnant

- H4_q10, H4_q09 (magnetized, Emag=2.42e47 erg, Bmax=(2-3)e15 G) 
   [from Ciolfi et al. 2017, https://arxiv.org/abs/1701.08738]
- H4_q10, H4_q08 (magnetized, Emag=(0.8-1)e41 erg, Bmax=2e12 G) 
   [from Kawamura et al. 2016, http://adsabs.harvard.edu/abs/2016PhRvD..94f4012K]



-- MORE DETAILS ON CUT WAVEFORMS --

===========[from Ciolfi et al. 2017]================

APR4_q10 - 5-6 orbits, cut ~40 ms of postmerger signal 
      MDC.SetInjHrss(2.5390073819e-19);  // hrss of the signal @ 10 kpc

APR4_q09 - 5-6 orbits, cut ~40 ms of postmerger signal
      MDC.SetInjHrss(2.52921276457e-19);  // hrss of the signal @ 10 kpc

MS1_q10 - ~4 orbits, cut ~50 ms of postmerger signal
      MDC.SetInjHrss(1.98543494297e-19);  // hrss of the signal @ 10 kpc

MS1_q09 - 3-4 orbits, cut ~50 ms of postmerger signal
      MDC.SetInjHrss(1.97456412978e-19);  // hrss of the signal @ 10 kpc

H4_q10 - ~4 orbits, cut ~22 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.15085254327e-19);  // hrss of the signal @ 10 kpc

H4_q09 - ~4 orbits, cut ~28 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.17470192371e-19);  // hrss of the signal @ 10 kpc

===========[from Kawamura et al. 2016]==============

H4_q10 - 5-6 orbits, cut ~12 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.48462919499e-19);  // hrss of the signal @ 10 kpc

H4_q08 - 5-6 orbits, cut ~25 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.44153516602e-19);  // hrss of the signal @ 10 kpc

===========[from Endrizzi et al. 2016]===============

LM_B0 - ~8 orbits, cut ~15 ms of postmerger signal
      MDC.SetInjHrss(2.40883554255e-19);  // hrss of the signal @ 10 kpc

UM_B0 - ~6 orbits, cut ~22 ms of postmerger signal
      MDC.SetInjHrss(2.52281689565e-19);  // hrss of the signal @ 10 kpc

===========[from Kastaun et al. 2016]================

SHT - 3-4 orbits, cut ~16 ms of postmerger signal 
      MDC.SetInjHrss(2.07441954e-19);  // hrss of the signal @ 10 kpc


