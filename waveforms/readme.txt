— LIST OF WAVEFORMS —

SMNS or stable remnant:

- APR4_q10, APR4_q09, MS1_q10, MS1_q09 (magnetized, Emag=2.42e47 erg, Bmax=(2-3)e15 G) 
   [from Ciolfi et al. 2017, https://arxiv.org/abs/1701.08738]
- LM_B0, UM_B0 (unmagnetized, Bmax=0)
   [from Endrizzi et al. 2016, http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1604.03445]
- SHT (unmagnetized, Bmax=0) 
   [from Kastaun et al. 2016, http://adsabs.harvard.edu/abs/2016PhRvD..94d4060K]

HMNS remnant

- H4_q10, H4_q09 (magnetized, Emag=2.42e47 erg, Bmax=(2-3)e15 G) 
   [from Ciolfi et al. 2017, https://arxiv.org/abs/1701.08738]
- H4_q10, H4_q08 (magnetized, Emag=(0.8-1)e41 erg, Bmax=2e12 G) 
   [from Kawamura et al. 2016, http://adsabs.harvard.edu/abs/2016PhRvD..94f4012K]
- HM_B0 (unmagnetized, Bmax=0)
   [from Endrizzi et al. 2016, http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1604.03445]
- LS220* (unmagnetized, Bmax=0; LS220_M1.5-S has initial spin)
   [from Kastaun & Galeazzi 2015, http://adsabs.harvard.edu/abs/2015PhRvD..91f4027K]
- SHT_M2* (unmagnetized, Bmax=0; SHT_M2.0-S has initial spin)
   [from Kastaun & Galeazzi 2015, http://adsabs.harvard.edu/abs/2015PhRvD..91f4027K]
- IF_2010 (unmagnetized, Bmax=0)
   [model in Fig. A1 in Rezzolla et al. 2010, http://adsabs.harvard.edu/abs/2010CQGra..27k4105R]



-- MORE DETAILS ON WAVEFORMS --

===========[ideal fluid]=============================

IF_2010 - ~3 orbits, ~120 ms of postmerger signal
      MDC.SetInjHrss(9.23636770353e-19);  // hrss of the signal @ 10 kpc   [ hrss@100Mpc= sqrt( int (hplus^2 + hcross^2)*dt ) ]

===========[from Ciolfi et al. 2017]================

APR4_q10 - 5-6 orbits, ~40 ms of postmerger signal 
      MDC.SetInjHrss(2.77670307575e-19);  // hrss of the signal @ 10 kpc

APR4_q09 - 5-6 orbits, ~40 ms of postmerger signal
      MDC.SetInjHrss(2.80063889587e-19);  // hrss of the signal @ 10 kpc

MS1_q10 - ~4 orbits, ~50 ms of postmerger signal
      MDC.SetInjHrss(2.34006699389e-19);  // hrss of the signal @ 10 kpc

MS1_q09 - 3-4 orbits, ~50 ms of postmerger signal
      MDC.SetInjHrss(2.22166814976e-19);  // hrss of the signal @ 10 kpc

H4_q10 - ~4 orbits, ~22 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.72453327126e-19);  // hrss of the signal @ 10 kpc

H4_q09 - ~4 orbits, ~28 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.67381171596e-19);  // hrss of the signal @ 10 kpc

===========[from Kawamura et al. 2016]==============

H4_q10 - 5-6 orbits, ~12 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.91825691261e-19);  // hrss of the signal @ 10 kpc

H4_q08 - 5-6 orbits, ~25 ms of postmerger signal and then collapse to a BH
      MDC.SetInjHrss(2.74543643199e-19);  // hrss of the signal @ 10 kpc

===========[from Endrizzi et al. 2016]===============

HM_B0 - 5-6 orbits, ~1 ms of postmerger signal and then collapse to a BH (prompt)
      MDC.SetInjHrss(2.63421396039e-19);  // hrss of the signal @ 10 kpc

LM_B0 - ~8 orbits, ~15 ms of postmerger signal
      MDC.SetInjHrss(2.60718740677e-19);  // hrss of the signal @ 10 kpc

UM_B0 - ~6 orbits, ~22 ms of postmerger signal
      MDC.SetInjHrss(2.82903545819e-19);  // hrss of the signal @ 10 kpc

===========[from Kastaun et al. 2016]================

SHT - 3-4 orbits, ~16 ms of postmerger signal 
      MDC.SetInjHrss(2.45063463256e-19);  // hrss of the signal @ 10 kpc

===========[from Kastaun & Galeazzi 2015]================

LS220_M1.5-I - ~10 orbits, ~9 ms of postmerger signal and then collapse to a BH 

LS220_M1.5-S - ~11 orbits, ~8 ms of postmerger signal and then collapse to a BH

LS220_M1.7-I - 8-9 orbits, <1 ms of postmerger signal and then collapse to a BH (prompt)

LS220_M1.8-I - ~8 orbits, <1 ms of postmerger signal and then collapse to a BH (prompt)

SHT_M2.0-I - ~6 orbits, ~17 ms of postmerger signal (collapse to a BH after the end of the simulation)

SHT_M2.0-S - ~7 orbits, ~9 ms of postmerger signal (collapse to a BH after the end of the simulation)

SHT_M2.2-I - ~5 orbits, <1 ms of postmerger signal and then collapse to a BH (prompt)













